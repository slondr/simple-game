use std::io::{self, Write};
use rand::Rng;

pub fn d(sides: i8) -> i8  {
    rand::thread_rng().gen_range(1, sides + 1)
}

pub fn you(s: &'static str) {
    println!("You {}", s);
}

pub fn clear() {
    // clear the screen and return the cursor to the origin
    print!("{esc}[2J{esc}[1;1H", esc = 27 as char);
}

pub fn prompt() -> String {
    println!("[0] Weak Attack\n[1] Strong Attack");
    let mut buf = String::new();
    io::stdin().read_line(&mut buf).unwrap();
    buf
}

fn print_inline(s: &'static str) {
    print!("{}", s);
    io::stdout().flush().unwrap();
}

pub fn prompt_inline(s: &'static str) -> String {
    let mut buf = String::new();
    print_inline(s);
    io::stdin().read_line(&mut buf).unwrap();
    buf
}
