mod entity;
use entity::{AttackType, Entity};

mod util;
use util::{d, you, clear, prompt, prompt_inline};

fn pre_game() {
    println!("Hello, welcome to simple-game!");
}

fn round(mut player: &mut Entity, mut enemy: &mut Entity) {
    while player.hp > 0 && enemy.hp > 0 {
	let attack = match prompt().trim().parse::<u8>().unwrap() {
	    0 => AttackType::Weak,
	    1 => AttackType::Strong,
	    _ => panic!("Invalid attack type")
	};
	clear();

	// player attacks enemy
	you("attack!");
	player.attack(&mut enemy, attack);
	if enemy.hp <= 0 {
	    // enemy is dead
	    you("win!");
	    break;
	}

	// enemy is alive and attacks player
	println!("\n{} attacks!", enemy.name);
	let enemy_attack_type = if d(2) == 2 { AttackType::Strong } else { AttackType::Weak };
	println!("{} uses a {} attack!", enemy.name, enemy_attack_type);
	enemy.attack(&mut player, enemy_attack_type);
	if player.hp <= 0 {
	    // player is dead
	    you("die!");
	    break;
	}

	// nobody died; continue the round
	println!("\nYour HP: {}\n{} HP: {}\n", player.hp, enemy.name, enemy.hp);
    }
}

fn game() {
    let name = prompt_inline("Enter your name: ");
    
    let mut player = Entity::new(name, 20, 10);

    loop {
	let mut enemy = Entity::new(String::from("Enemy"), 20, 8);
	
	round(&mut player, &mut enemy);

	let choice = prompt_inline("\nThanks for playing!\nPlay again? [y/n] ");
	if !choice.starts_with("y") {
	    break
	} else {
	    player.hp = 20
	}
    }
}

fn post_game() {
    println!("Goodbye!")
}

fn main() {
    pre_game();

    game();

    post_game();
}
