use crate::d;
use std::fmt;

pub enum AttackType {
    Weak,
    Strong
}

impl AttackType {
    fn attack(&self) -> i8 {
	match self {
	    AttackType::Weak => d(20),
	    AttackType::Strong => d(20) - 3
	}
    }
    
    fn damage(&self) -> i8 {
	match self  {
	    AttackType::Weak => d(8),
	    AttackType::Strong => d(8) + 3
	}
    }
}

impl fmt::Display for AttackType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
	write!(f, "{}", match self {
	    AttackType::Weak => "weak",
	    AttackType::Strong => "strong"
	})
    }
}

pub struct Entity {
    pub name: String,
    pub hp: i8,
    pub dt: i8
}

impl Entity {
    #[inline]
    pub fn new(name: String, hp: i8, dt: i8) -> Entity {
	Entity { name, hp, dt }
    }
    
    pub fn attack(&mut self, target: &mut Entity, attack_type: AttackType) {
	// roll for attack
	if attack_type.attack() > target.dt {
	    // hit; roll for damage
	    let damage = attack_type.damage();
	    println!("{} damage!", damage);
	    target.hp -= damage;
	} else {
	    // miss
	    println!("Miss!");
	}
    }
}
